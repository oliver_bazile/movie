package android.test.movie.data.Api

import android.movie.movietest.domaine.model.Movie
import android.test.movie.util.Constants
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface MovieApi {

    @GET("popular?")
    suspend fun getMovie(
        @Query("api_key") api_key: String = Constants.API_KEY,
        @Query("language") language: String = "fr",
        @Query("page") page: Int
    ) : Response<Movie>
}