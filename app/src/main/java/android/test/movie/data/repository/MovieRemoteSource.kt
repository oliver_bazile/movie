package android.test.movie.data.repository

import android.movie.movietest.domaine.model.Movie
import retrofit2.Response

interface MovieRemoteSource {

    suspend fun getMovie(page:Int): Response<Movie>
}