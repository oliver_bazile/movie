package android.test.movie.data.repository

import android.movie.movietest.domaine.model.Movie
import android.test.movie.data.Api.MovieApi
import retrofit2.Response

class MovieRemoteSourceImpl(private val movieApi: MovieApi): MovieRemoteSource {
    override suspend fun getMovie(page: Int): Response<Movie> {
        return  movieApi.getMovie(page = page)
    }
}