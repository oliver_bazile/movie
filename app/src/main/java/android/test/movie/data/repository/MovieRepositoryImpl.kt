package android.test.movie.data.repository

import android.movie.movietest.domaine.model.Movie
import android.test.movie.domaine.repository.MovieRepository
import android.test.movie.util.Resource
import retrofit2.Response

class MovieRepositoryImpl(private  val movieRemoteSource: MovieRemoteSource) : MovieRepository {
    override suspend fun getMovie(page: Int): Resource<Movie> {
        return  responseToResource(movieRemoteSource.getMovie(page))
    }

    private fun responseToResource(response: Response<Movie>):Resource<Movie>{
        if(response.isSuccessful){
            response.body()?.let {result->
                return Resource.Success(result)
            }
        }
        return Resource.Error(response.message())
    }
}