package android.test.movie.domaine.repository

import android.movie.movietest.domaine.model.Movie
import android.test.movie.util.Resource

interface MovieRepository {
    suspend fun  getMovie(page:Int) : Resource<Movie>
}