package android.test.movie.domaine.user_case

import android.movie.movietest.domaine.model.Movie
import android.test.movie.domaine.repository.MovieRepository
import android.test.movie.util.Resource

class GetMovie(private val movieRepository: MovieRepository) {

    suspend fun execute(page:Int) : Resource<Movie>{
        return movieRepository.getMovie(page)
    }
}