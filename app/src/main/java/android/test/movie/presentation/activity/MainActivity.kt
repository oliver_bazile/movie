package android.test.movie.presentation.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.test.movie.R
import android.test.movie.databinding.ActivityMainBinding
import android.test.movie.presentation.adapter.MovieAdapter
import android.test.movie.presentation.viewModel.ViewModelFactoryMovie
import android.test.movie.presentation.viewModel.ViewModelMovie
import androidx.lifecycle.ViewModelProvider
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    @Inject
    lateinit var factory: ViewModelFactoryMovie
    @Inject
    lateinit var movieAdapter: MovieAdapter
    lateinit var viewModelMovie: ViewModelMovie
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewModelMovie = ViewModelProvider(this, factory).get(ViewModelMovie::class.java)

    }
}