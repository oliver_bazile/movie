package android.test.movie.presentation.adapter

import android.movie.movietest.domaine.model.Result
import android.os.Bundle
import android.test.movie.R
import android.test.movie.databinding.RecyclerViewMoviesBinding
import android.test.movie.util.Constants
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import java.lang.StringBuilder

class MovieAdapter : RecyclerView.Adapter<MovieAdapter.MovieViewHolder>() {
    inner class MovieViewHolder(private val binding: RecyclerViewMoviesBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(result: Result) {
            val urlImage = StringBuilder().apply {
                append(Constants.URL_IMAGE)
                append(result.backdrop_path)
            }

            Glide.with(binding.imageViewMovieFragment.context)
                .load(urlImage.toString())
                .into(binding.imageViewMovieFragment)

            binding.textViewDetailsMovieFragment.text = result.overview
            binding.textViewTitleMovieFragment.text = result.title

            binding.root.setOnClickListener {
                onItemClickListener?.let {
                    it(result)
                }
            }
        }
    }


    private val callback = object : DiffUtil.ItemCallback<Result>() {
        override fun areItemsTheSame(oldItem: Result, newItem: Result): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Result, newItem: Result): Boolean {
            return oldItem == newItem
        }
    }

    var differ = AsyncListDiffer(this, callback)

    private var onItemClickListener: ((Result) -> Unit)? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val binding =
            RecyclerViewMoviesBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MovieViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val movieCurrent = differ.currentList[position]
        holder.bind(movieCurrent)
        holder.itemView.setOnClickListener { view ->
            val bundle = Bundle().apply {
                putSerializable("details_movie", movieCurrent)
            }
            Navigation.findNavController(view)
                .navigate(R.id.action_movieFragment_to_detailsMovieFragment, bundle);

        }

    }

    override fun getItemCount(): Int = differ.currentList.size
}