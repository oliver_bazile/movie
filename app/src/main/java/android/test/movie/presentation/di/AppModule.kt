package android.test.movie.presentation.di

import android.app.Application
import android.test.movie.data.Api.MovieApi
import android.test.movie.data.repository.MovieRemoteSource
import android.test.movie.data.repository.MovieRemoteSourceImpl
import android.test.movie.data.repository.MovieRepositoryImpl
import android.test.movie.domaine.repository.MovieRepository
import android.test.movie.domaine.user_case.GetMovie
import android.test.movie.presentation.adapter.MovieAdapter
import android.test.movie.presentation.viewModel.ViewModelFactoryMovie
import android.test.movie.util.Constants
import android.widget.ViewSwitcher
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideMovieRetrofit(): Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(Constants.BASE_URL)
            .build()

    }

    @Provides
    @Singleton
    fun providersMovieApi( retrofit: Retrofit): MovieApi {
        return retrofit.create(MovieApi::class.java)
    }

    @Provides
    @Singleton
    fun providerMovieRemote(api: MovieApi): MovieRemoteSource {
        return MovieRemoteSourceImpl(api)
    }

    @Singleton
    @Provides
    fun providerMovieRepository(
        movieRemoteSource: MovieRemoteSource) : MovieRepository {
        return MovieRepositoryImpl(movieRemoteSource)
    }

    @Singleton
    @Provides
    fun providerMovieUserCase(movieRepository: MovieRepository): GetMovie {
        return GetMovie(movieRepository)
    }

    @Singleton
    @Provides
    fun providerFactoryViewModel(application: Application, getMovie: GetMovie): ViewModelFactoryMovie{
        return ViewModelFactoryMovie(application, getMovie)
    }

    @Singleton
    @Provides
    fun provideMovieAdapter(): MovieAdapter {
        return MovieAdapter()
    }

}