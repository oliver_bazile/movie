package android.test.movie.presentation.fragment

import android.movie.movietest.domaine.model.Result
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.test.movie.R
import android.test.movie.databinding.FragmentDetailsMovieBinding
import android.test.movie.util.Constants
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.bumptech.glide.Glide
import java.lang.StringBuilder

/**
 * A simple [Fragment] subclass.
 */
class DetailsMovieFragment : Fragment() {

    var movieResult = MutableLiveData<Result>()

    private lateinit var binding: FragmentDetailsMovieBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_details_movie, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentDetailsMovieBinding.bind(view)
        movieResult.value = arguments?.getSerializable("details_movie") as Result?
        bindCharacter(movieResult.value)
    }

    private fun bindCharacter(result: Result?) {
        val urlImage = StringBuilder().apply {
            append(Constants.URL_IMAGE)
            append(result?.poster_path)
        }

        context?.let {
            Glide.with(it)
                .load(urlImage.toString())
                .into(binding.imageViewDetailsMovie)
        }

        binding.textViewTitleDetailsMovie.text = result?.title
        binding.textViewDateDetailsMovie.text = result?.release_date
        binding.textViewTitleDetailsMovie.text = result?.title
        binding.textViewOverviewDetailsMovie.text = result?.overview
        binding.textViewPopularityDetailsMovie.text =  String.format("Popularity : %.2f",result?.popularity)
        binding.textViewVoteDetailsMovie.text =  String.format("Vote : %.2f",result?.vote_average)

    }

}