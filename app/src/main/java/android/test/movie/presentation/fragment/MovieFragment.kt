package android.test.movie.presentation.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.test.movie.R
import android.test.movie.databinding.FragmentMovieBinding
import android.test.movie.presentation.activity.MainActivity
import android.test.movie.presentation.adapter.MovieAdapter
import android.test.movie.presentation.viewModel.ViewModelMovie
import android.test.movie.util.Resource
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager


/**
 * A simple [Fragment] subclass.
 */
class MovieFragment : Fragment() {

    private lateinit var binding: FragmentMovieBinding
    lateinit var movieAdapterFragment: MovieAdapter
    private lateinit var viewModel: ViewModelMovie
    var page = 1

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_movie, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentMovieBinding.bind(view)
        movieAdapterFragment = (activity as MainActivity).movieAdapter
        viewModel= (activity as MainActivity).viewModelMovie
        initRecyclerView()
        getMovie()
    }

    private fun initRecyclerView() {
        binding.recyclerViewMoviesFragment.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = movieAdapterFragment

        }
    }

    private fun getMovie() {
        viewModel.getMovie(page)
        viewModel.listMovie.observe(viewLifecycleOwner,) { response ->
            when (response) {
                is Resource.Loading -> {
                    showProgressBar(true)

                }
                is Resource.Success -> {
                    showProgressBar(false)
                    response.data.let {
                        movieAdapterFragment.differ.submitList(it?.results?.toList())
                    }

                    Log.w("Movie", "Text ${movieAdapterFragment.differ.currentList.size}")

                }
                is Resource.Error -> {
                    showProgressBar(false)
                    response.message?.let {
                        Toast.makeText(activity, "An error occurred : $it", Toast.LENGTH_LONG)
                            .show()
                    }
                }
            }
        }
    }

    private fun showProgressBar(showProgress: Boolean) {
        if (showProgress)
            binding.progressBarMovies.visibility = View.VISIBLE
        else binding.progressBarMovies.visibility = View.GONE
    }
}