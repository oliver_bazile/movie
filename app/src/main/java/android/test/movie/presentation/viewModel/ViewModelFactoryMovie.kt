package android.test.movie.presentation.viewModel

import android.app.Application
import android.test.movie.domaine.user_case.GetMovie
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class ViewModelFactoryMovie(val application: Application, val getMovie: GetMovie) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ViewModelMovie(application, getMovie) as T
    }
}
