package android.test.movie.presentation.viewModel

import android.app.Application
import android.content.Context
import android.movie.movietest.domaine.model.Movie
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.test.movie.domaine.user_case.GetMovie
import android.test.movie.util.Resource
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

@HiltViewModel
class ViewModelMovie @Inject constructor(private val app: Application,
private var getMovie: GetMovie) : AndroidViewModel(app) {
    val listMovie: MutableLiveData<Resource<Movie>> = MutableLiveData()

    fun getMovie(page: Int) {
        viewModelScope.launch {
            listMovie.postValue(Resource.Loading())
            try {
                if (isNetworkAvailable(app)) {
                    val result= getMovie.execute(page)
                    listMovie.postValue(result)
                } else {
                    listMovie.postValue(Resource.Error("Internet is not available"))
                }
            } catch (e: Exception) {
                listMovie.postValue(Resource.Error(e.message.toString()))
            }
        }
    }

    private fun isNetworkAvailable(context: Context?): Boolean {
        if (context == null) return false
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val capabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                when {
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                        return true
                    }
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                        return true
                    }
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
                        return true
                    }
                }
            }
        } else {
            val activeNetworkInfo = connectivityManager.activeNetworkInfo
            if (activeNetworkInfo != null && activeNetworkInfo.isConnected) {
                return true
            }
        }
        return false
    }
}