package android.test.movie.presentation.widget

import android.content.Context
import android.test.movie.R
import android.util.AttributeSet
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat

class TextViewBoldCustom @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : androidx.appcompat.widget.AppCompatTextView(context, attrs) {
    init {
        this.typeface = ResourcesCompat.getFont(context, R.font.josefin_bold)
    }
}